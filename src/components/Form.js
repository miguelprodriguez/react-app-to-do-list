import React, { useState } from 'react';
import { Container, Jumbotron, Form, Button } from 'react-bootstrap';

export default function Todo ({data}) { // props input will be coming from the app
	const [ value, setValue ] = useState('');

	function addTask (e) {
		e.preventDefault();
		setValue(''); // clear values
		data(value);
	}

	return(
		<div>
		<Container>
			<Jumbotron>
				<h1>To Do List</h1>
			    <Form onSubmit={addTask}>
					<Form.Control value={value} onChange={e => setValue(e.target.value)} required/>
					<br />
					<Button type="submit" block variant="success">Submit</Button>
				</Form>
			</Jumbotron>
		</Container>
		</div>
		)

}