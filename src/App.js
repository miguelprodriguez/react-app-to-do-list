import React, { useState } from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';

import Form from './components/Form';
import { Container, Jumbotron, Button, Row, Col } from 'react-bootstrap';
import { GoTrashcan } from 'react-icons/go';

function App() {
  const [ todos, setTodos ] = useState([]);

  console.log(todos);

  const done = index => setTodos(
    // index will be coming from the input of the onClick function, hence if the current from the map is clicked from the index, the complete status will be toggled
    todos.map((todo, target) => target === index ? {...todo, complete: !todo.complete} : todo));

  	return (
  		<div>
        {/* set the array state hook above based on the data from Form.js */}
  			<Form data={text => setTodos([...todos, { text, complete: false }])} /> {/* ...todos keep the original prior */}
        <div>
          <Container>
             <Jumbotron>
          {/* get the todos state hook to show all the tasks */}
          {/* complete is destructured from the todos useState for the CSS property below */}
              {todos.map(({text, complete}, i) => (
                  <div key={text} style={{ textDecoration: complete ? "line-through" : "" }}>
                    <Container>
                      <Row>
                        <Col className="my-2"> {text} </Col> 
                        <Col >
                          <Button onClick={(e) => done(i)} style={{ float: "right"}}> Done </Button>
                        </Col>
                      </Row>
                    </Container>
                  </div>
                ))}
              { /* Set array to empty */}
              <Button className="mt-5" onClick={() => setTodos([])} variant="danger" block><GoTrashcan/>Remove All</Button>
            </Jumbotron>
          </Container>
        </div>
  		</div>
  	);
}

export default App;
